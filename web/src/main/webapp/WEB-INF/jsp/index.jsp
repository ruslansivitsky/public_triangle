<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>DCT :: Test service</title>
    <meta charset="UTF-8">
    <meta name=description content="">
    <meta name=viewport content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/resources/vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/vendor/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="/resources/vendor/css/theme.css" rel="stylesheet">
</head>

<body role="document">
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">DisCoTech</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container theme-showcase" role="main">
    <div class="jumbotron">
        <h1>Welcome to Triangle Test</h1>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Overview</h3>
        </div>
        <div class="panel-body">
            <div class="media">
                <img class="media-object pull-left" src="/resources/vendor/triangle.png">

                <div class="media-body">
                    <h4 class="media-heading">Triangle</h4>
                    A triangle is a polygon with three edges and three vertices. It is one of the basic shapes in
                    geometry. In mathematics, the triangle inequality states that for any triangle, the sum of the
                    lengths of any two sides must be greater than length of the remaining side. Our service checks that
                    for given sides <strong>a</strong>,<strong>b</strong> and <strong>c</strong> the triangle is exists.
                </div>
            </div>

            <p> We have prepared Java Interface for this service and implemented it. <code>com.dct.service.TriangleService</code>
                is interface describing contract for triangle checking. It's implementation <code>com.dct.service.impl.TriangleServiceImpl</code>
                can be found in the <strong>triangle-service-1.0-SNAPSHOT.jar</strong> library.
            </p>

            <p> Also we prepared a simple REST service providing the same operation and returning the decision if
                triangle exists or not.
            </p>

            <div class="well">
                So here is application and you are QA engineer, aren't you?
            </div>
            <div class="list-group">
                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">1. Create test plans for both Java and REST services</h4>

                    <p class="list-group-item-text">A test plan documents the strategy that will be used to verify and
                        ensure that a product or system meets its design specifications and other requirements. A test
                        plan is usually prepared by or with significant input from test engineers.</p>
                </a>
                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">2. Create UnitTest for provided TriangleService
                        implementation</h4>

                    <p class="list-group-item-text">In computer programming, unit testing is a software testing method
                        by which individual units of source code, sets of one or more computer program modules together
                        with associated control data, usage procedures, and operating procedures are tested to determine
                        if they are fit for use.[1] Intuitively, one can view a unit as the smallest testable part of an
                        application.</p>
                </a>
                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">3. Create Automated Test for checkTriangle REST service</h4>

                    <p class="list-group-item-text">Representational state transfer (REST) is an abstraction of the
                        architecture of the World Wide Web. More precisely, REST is an architectural style consisting of
                        a coordinated set of architectural constraints applied to components, connectors, and data
                        elements, within a distributed hypermedia system. REST ignores the details of component
                        implementation and protocol syntax in order to focus on the roles of components, the constraints
                        upon their interaction with other components, and their interpretation of significant data
                        elements.</p>
                </a>
                <a href="#" class="list-group-item alert-warning">
                    <h4 class="list-group-item-heading">4*. Create Load Test for checkTriangle service</h4>

                    <p class="list-group-item-text">Yes, this task is <strong>additional</strong>, we do not push on
                        you, but if you so curious, we cannot stop you.
                    </p>
                </a>
            </div>
        </div>
    </div>

    <form:form method="post" commandName="triangleModel">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">REST Services</h3>
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Service</th>
                    <th>Method</th>
                    <th>Params</th>
                    <th>Comments</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <form:form method="get">
                        <a href="<spring:url value="http://localhost:8080/service/version" htmlEscape="true" />">/service/version</a>
                        </form:form>
                    </td>
                    <td>GET</td>
                    <td>
                        Request:
                        <pre>N/A</pre>
                        Response:
                        <pre>${version}</pre>
                    </td>
                    <td>Returns application version</td>
                </tr>
                <tr>
                    <td><form:textarea class="form-control" path="a"
                                       />${trianglemodel.a}
                        <form:errors path="a" cssClass="error"/>
                        <form:textarea class="form-control" path="b"
                                       />${trianglemodel.b}
                        <form:errors path="b" cssClass="error"/>
                        <form:textarea class="form-control" path="c"
                                     />${trianglemodel.c}
                        <form:errors path="c" cssClass="error"/></td>
                    <td>
                        <button type="submit" class="btn btn-primary pull-right">
                            <span class="glyphicon glyphicon-tasks"></span> /service/checkTriangle
                        </button>
                    </td>
                    <td>POST</td>
                    <td>
                        Response:
                        <pre><form:textarea path="exists" class="form-control" charset="utf-8"/>${exists}</pre>
                    </td>
                    <td>Checks triangle existence. Takes it's sides lengths as parameters. Consumes JSON,
                        Produces JSON
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
    </form:form>

    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Hints</h3>
        </div>
        <div class="panel-body">
            <ul>
                <li>
                    Start with creating <strong>test plan</strong>, what you going to do is more important for us than
                    how you going to do it.
                </li>
                <li>
                    If you do not know how to start - proper words to <strong>google</strong> are:
                    <mark>Java</mark>
                    ,
                    <mark>JUnit</mark>
                    ,
                    <mark>TestNG</mark>
                    ,
                    <mark>HTTP</mark>
                    ,
                    <mark>Maven</mark>
                    ,
                    <mark>RESTful service</mark>
                    ,
                    <mark>JSON</mark>
                    ,
                    <mark>Postman REST client</mark>
                    ,
                    <mark>SoapUI</mark>
                    ,
                    <mark>JMeter</mark>
                    ,
                    <mark>Groovy</mark>
                </li>
                <li>Just send reports to the same email you receive the task from.</li>
                <li>Do the task yourself please. We need your knowledge, not collective mind. Do not hesitate to take
                    the knowledge from any source you have found, but be sure it <strong>is</strong> knowledge.
                </li>
                <li>
                    Yes, You may ask any additional questions by email. Yes, English is better.
                </li>
            </ul>
        </div>
    </div>
</div>

<footer>
    <div class="container text-center disabled">
        <small>© DCT, Prague. 2014.</small>
    </div>
</footer>

<script src="//code.jquery.com/jquery.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>

</body>
</html>