package com.sivitsky.ruslan.client.impl;

import com.sivitsky.ruslan.client.TriangleService;

import com.sun.jersey.api.client.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by ruslan on 28/11/14.
 */
@Service
public class TriangleServiceImpl implements TriangleService {

    private Client client;
    @Value("${base.url}")
    private String BaseUrl;

    public void setBaseUrl(String baseUrl) {
        BaseUrl = baseUrl;
    }

    public String getBaseUrl() {
        return BaseUrl;
    }

   // @Value("${version}")
    private String version;

    public TriangleServiceImpl() {
        client = Client.create();
    }


    @Override
    public String checkTriangle(double a, double b, double c) throws UnsupportedEncodingException {
        String jsonStringTriangle = "{"  + "\"a\":" + "\"" + Double.toString(a) +"\","
                + "\"b\":" + "\"" + Double.toString(b) +"\","
                + "\"c\":" + "\"" + Double.toString(c) + "\"" + "}";
        WebResource webResource = client.resource(BaseUrl+"/checkTriangle");
        String result = webResource.accept("application/json").header("Content-Type", MediaType.APPLICATION_JSON_TYPE).post(String.class, jsonStringTriangle);
        return result;
    }


    @Override
    public String getTriangleVersion(){
        String result = version;
        return result;
    }

    public static void main(String arg[]) throws UnsupportedEncodingException {
        TriangleServiceImpl triangleService = new TriangleServiceImpl();
        if (triangleService.getBaseUrl()==null){
            triangleService.setBaseUrl(arg[3]);
        }
        String result = triangleService.checkTriangle(Double.parseDouble(arg[0]), Double.parseDouble(arg[1]), Double.parseDouble(arg[2]));
        System.out.println("EXIST: "+result);
    }
}
