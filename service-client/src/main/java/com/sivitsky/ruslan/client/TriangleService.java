package com.sivitsky.ruslan.client;

import java.io.UnsupportedEncodingException;

/**
 * Created by ruslan on 28/11/14.
 */
public interface TriangleService {
    public String checkTriangle(double a, double b, double c) throws UnsupportedEncodingException;
    public String getTriangleVersion();
}