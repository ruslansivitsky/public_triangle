package com.sivitsky.ruslan.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.io.*;
import java.util.Properties;

/**
 * Created by ruslan on 02/12/14.
 */

@Path("/version")
public class TriangleVersion {
   /* private static final String sFileName = "version.properties";
    private static String sDirSeparator = System.getProperty("file.separator");
    private static Properties props = new Properties();

    public String RecieveVersion() throws IOException {
        File currentDir = new File(".");
        try {
            String sFilePath = currentDir.getCanonicalPath() + sDirSeparator + sFileName;
            FileInputStream ins = new FileInputStream(sFilePath);
            props.load(ins);
            return (String) props.getProperty("version");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return " - ";
    }

    public String getAPIVersion() {
        String path = "/version.properties";
        InputStream stream = getClass().getResourceAsStream(path);
        if (stream == null) return "UNKNOWN";
        Properties props = new Properties();
        try {
            props.load(stream);
            stream.close();
            return (String)props.get("version");
        } catch (IOException e) {
            return "UNKNOWN";
        }
    }*/

    @GET
    public Response getMsg() throws IOException {
       // TriangleVersion triangleVersion = new TriangleVersion();
       // String version = triangleVersion.getAPIVersion();
        String version = TriangleVersion.class.getClass().getPackage().getImplementationVersion();
        return Response.status(200).entity(version).build();
    }
}