package com.sivitsky.ruslan.service;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Created by ruslan on 30/11/14.
 */
@Path("/checkTriangle")
public class TriangleResourse {
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String checkTriangle(String triangleParameters) throws IOException, ParseException {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(triangleParameters);
        double a = Double.parseDouble((String)jsonObject.get("a"));
        double b = Double.parseDouble((String)jsonObject.get("b"));
        double c = Double.parseDouble((String)jsonObject.get("c"));
        return isTriangle(a, b, c);
    }

    public String jsonToObject(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String obj = mapper.readValue(json, String.class);
        return obj;
    }

    public String isTriangle(double a, double b, double c){
        if ((a+b>c)&(b+c>a)&(a+c>b))
        {
            return "{\"exists\":\"YES\"}";
        }
        else
        {
            return "{\"exists\":\"NO\"}";
        }
    }
}